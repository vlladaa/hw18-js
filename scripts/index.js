let randomUser = {
  name: 'Evangeline',
  lastName: 'Martinez',
  motherCountry: 'Canada',
  hometown: 'Toronto',
  visitedCounties: ['Germany', 'Norway', 'France'],
  souvenirs: {
    germany: 'sausage',
    norway: 'viking figurine',
    france: ['cheese', 'wine', 'baguette']
  }
}

const cloneObject = (object) => {
  let copy = {};

  if (typeof object !== 'object' || object === null) {
    return object
  } else {
    for (let key in object) {
      if (typeof object[key] != 'object') {
        copy[key] = object[key]
      } else {
        copy[key] = cloneObject(object[key])
      }
    }

    return copy;
  }
}

const secondUser = cloneObject(randomUser);
secondUser.pet = 'cat'

console.log(cloneObject(randomUser))
console.log(secondUser)
